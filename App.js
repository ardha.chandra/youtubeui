import React from 'react';
import { StyleSheet, Text, View } from 'react-native';
import Component from './Latihan/Component/Component';
import YoutubeUI from './Tugas/Tugas12/App';
import LoginScreen from './Tugas/Tugas13/LoginScreen';
import RegisterScreen from './Tugas/Tugas13/RegisterScreen';
import AboutScreen from './Tugas/Tugas13/AboutScreen';
import Tugas14 from './Tugas/Tugas14/App';
import Navi from './Latihan/ReactNavigation/index'



export default function App() {
  return (
    <Navi />
    //<ReactNavigation />
    //<RegisterScreen/>
    //<Tugas14/>
    //<AboutScreen/>
    //<YoutubeUI />
    //<Component />
    /*
    <View style={styles.container}>
      <Text>Open up App.js to start working on your app!</Text>
    </View> 
    */
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
});

